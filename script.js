//template variable
const template = document.createElement("template");

//styles and html structure for template
template.innerHTML = `
<style>

* {
    font-family: "Arial Bold MT";
}
.listree-submenu-heading {
    cursor: pointer;
}

ul.listree {
    list-style: none;
}

ul.listree-submenu-items {
    list-style: none;
    white-space: nowrap;
    margin-right: 4px;
    padding-left: 20px;
}

div.listree-submenu-heading.collapsed:before {
    content: "+";
    margin-right: 4px;
}

div.listree-submenu-heading.expanded:before {
    content: "-";
    margin-right: 4px;
}
</style>

<ul class="listree">
  <li>
    <div class="listree-submenu-heading">Parcel</div>
    <ul class="listree-submenu-items">
      <li>
        <div class="listree-submenu-heading">Size and Location</div>
        <ul class="listree-submenu-items"></ul>
      </li>
      <li>
        <div class="listree-submenu-heading">Permits and Deeds</div>
        <ul class="listree-submenu-items"></ul>
      </li>
    </ul>
  </li>
  <li></li>
  <li>
    <div class="listree-submenu-heading">Description</div>
    <ul class="listree-submenu-items"></ul>
  </li>
  <li>
    <div class="listree-submenu-heading">Assessments</div>
    <ul class="listree-submenu-items"></ul>
  </li>
  <li></li>
  <li>
    <div class="listree-submenu-heading">Ownership</div>
    <ul class="listree-submenu-items">
      <li>
        <div class="listree-submenu-heading">Owners</div>
        <ul class="listree-submenu-items"></ul>
      </li>
      <li>
        <div class="listree-submenu-heading">Tax Bill/3rd Party Address</div>
        <ul class="listree-submenu-items"></ul>
      </li>
      <li>
        <div class="listree-submenu-heading">Bank Address</div>
        <ul class="listree-submenu-items"></ul>
      </li>
    </ul>
  </li>
  <li></li>
  <li>
    <div class="listree-submenu-heading">Images</div>
    <ul class="listree-submenu-items"></ul>
  </li>
  <li>
    <div class="listree-submenu-heading">GIS</div>
    <ul class="listree-submenu-items"></ul>
  </li>
  <li>
    <div class="listree-submenu-heading">Sites</div>
    <ul class="listree-submenu-items"></ul>
  </li>
</ul>
`;

class NestedMenu extends HTMLElement {
    connectedCallback() {

        //importing my template content
        const templateContent = document.importNode(template.content, true);
        //appending template content
        this.appendChild(templateContent);
        
        function listree() {
            const subMenuHeadings = document.getElementsByClassName("listree-submenu-heading");
    
            Array.from(subMenuHeadings).forEach(function (subMenuHeading) {
    
                subMenuHeading.classList.add("collapsed");
                subMenuHeading.nextElementSibling.style.display = "none";
    
                subMenuHeading.addEventListener('click', function (event) {
                    event.preventDefault();
                    const subMenuList = event.target.nextElementSibling;

                    if (subMenuList.style.display == "none") {
                        subMenuHeading.classList.remove("collapsed");
                        subMenuHeading.classList.add("expanded");
                        subMenuList.style.display = "block";
                    }
                    else {
                        subMenuHeading.classList.remove("expanded");
                        subMenuHeading.classList.add("collapsed");
                        subMenuList.style.display = "none";
                    }
                    event.stopPropagation();
                });
            });
        }
        listree();
    }
}

customElements.define("nested-menu", NestedMenu);